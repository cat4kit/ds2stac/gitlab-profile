<!--
SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

# DS2STAC

A Python package for harvesting and ingesting (meta)data into STAC-based catalog infrastructures

## Algorithm


![DS2STAC algorithm](https://codebase.helmholtz.cloud/cat4kit/ds2stac/ds2stac/-/raw/main/ds2stac-algorithm.png)

## Building this documentation

To build and modify this documentation yourself, clone the
[source code][source code] from gitlab and make your changes in the
[source](source) folder.

```bash
git clone https://codebase.helmholtz.cloud/cat4kit/ds2stac/ds2stac
```

You can then build the documentation by first
installing the necessary requirements via

```bash
pip install -r requirements.txt
```

Now start building the docs via

```bash
make html  # or make.bat html on windows
```

and open the file in `builds/html/index.html` with any browser.

[source code]: https://codebase.helmholtz.cloud/cat4kit/ds2stac/ds2stac

## Extensions

This documentation is based upon sphinx and uses the following extension
modules:

- [myst-parser][myst-parser] for rendering markdown
- [sphinx-design][sphinx-design] for tabs, cards, etc.
- [sphinx-rtd-theme][sphinx-rtd-theme] as theme


[myst-parser]: https://myst-parser.readthedocs.io
[sphinx-design]: https://sphinx-design.readthedocs.io
[sphinx-rtd-theme]: https://sphinx-rtd-theme.readthedocs.io


## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/sphinx-documentation-template.git.

See the template repository for instructions on how to update the skeleton for
this package.



## License information

Copyright © 2023 Karlsruher Institut für Technologie



Licensed under the EUPL-1.2

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.
