..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _stac:

=========
About STAC
=========

In this section we are going to talk about STAC-API (STAC-FastAPI), STAC-Dataset (pgSTAC),and STAC-Browser.


The catalog architecture utilized in our project is the `SpatioTemporal Assets Catalog (STAC) <https://stacspec.org>`_, which originated from the collaboration of satellite imagery suppliers. Over the past few years, there has been a significant growth in the user base and community surrounding the SpatioTemporal Asset Catalog (STAC). As a result, a diverse set of data providers have adopted STAC as their primary catalog architecture. A significant ecosystem has emerged around STAC, encompassing several extensions and plugins designed for a wide range of applications. These include visualization solutions for geospatial data, specialized databases specifically tailored for STAC, as well as client libraries available for major programming languages. Further information about this ecosystem can be found at https://stacindex.org/ecosystem. Currently, STAC presents itself as a "lingua franca" for the purpose of describing geographic data, hence facilitating its manipulation, organization, and retrieval. In this context, the framework is specifically designed to provide a customized structure for implementing a catalog and data exploration system, while requiring only a little amount of essential metadata. The fundamental structure of STAC revolves around Items, which are uncomplicated GeoJSONs containing all essential details pertaining to a dataset. Typically, a STAC Item necessitates a title, details pertaining to the bounding box, and temporal information.


.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: STAC (SpatioTemporal Asset Catalog)
