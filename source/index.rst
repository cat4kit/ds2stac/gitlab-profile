..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


..
   ds2stac documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

=========
DS2STAC:
=========

**A Python package for harvesting and ingesting (meta)data into STAC-based catalog infrastructures**


.. image:: https://codebase.helmholtz.cloud/cat4kit/ds2stac/gitlab-profile/badges/main/pipeline.svg
   :target: https://codebase.helmholtz.cloud/cat4kit/ds2stac/gitlab-profile/-/pipelines?page=1&scope=all&ref=main
.. image:: https://readthedocs.org/projects/ds2stac/badge/?version=latest
   :target: https://ds2stac.readthedocs.io/en/latest/
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/ds2stac/gitlab-profile
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/ds2stac/gitlab-profile
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.8086566.svg
   :target: https://doi.org/10.5281/zenodo.8086566


Overview:
=================

The DS2STAC (Data Servers/Services to `STAC metadata catalog <https://stacspec.org>`_) package comprises of three specialized sub-packages designed for the purpose of scanning and harvesting datasets, as well as generating STAC-Catalogs, STAC-Collections, and STAC-Items. Additionally, there is another package included in the DS2STAC suite that facilitates the ingestion of the generated STAC metadata into the STAC-database, such as pgstac. Each of these packages facilitates the retrieval of geospatial and temporal information required for the creation of SpatioTemporal Asset Catalog (STAC) Catalogs, Collections, and Items.
At present, the DS2STAC framework enables the extraction of data from THREDDS-Server, Intake-Catalogs, and SensorThings APIs. In each of the three instances, it establishes and oversees uniform STAC items, catalogs, and collections. These resources are subsequently made publicly accessible through the pgSTAC database and the STAC API, facilitating a user-friendly engagement with environmental research data inside the specified data servers and services.

The DS2STAC initiative is a component of the `CAT4KIT (Catalog service for Karlsruhe Institute of Technology) <https://codebase.helmholtz.cloud/cat4kit>`_ project. Cat4KIT has received funding from the `Exzellenzuniversitäts-Vorhaben Research Data Management <https://www.kit.edu/ps/vp-digitalisierung/do/exu-vorhaben-research-data-management_602.php>`_ of the Karlsruhe Institute of Technology.

As mentioned in above DS2STAC has four different submodules that each of them is responsilbe for harvesting and creating STAC-Catalogs, -Collections, and -Items from a specific data server/sevices or ingesting the creating the datasets in a STAC-database. We name these submodules as follows:


.. grid:: 1 2 2 2
   :gutter: 2

   .. grid-item-card:: TDS2STAC

      .. image:: _static/resized_image_tds2stac.png
         :width: 100%
         :alt: TDS2STAC logo
         :align: center

      This Python-based package is responsible for the harvesting and generation of STAC-Metadata from a THREDDS. The package possesses the capability to extract detailed datasets information from three distinct web services, namely ISO, WMS, and ncML.

      .. tip:: To access information regarding the utilization of a TDS2STAC, please refer to the TDS2STAC documentation available at the following link:|br| `<https://tds2stac.readthedocs.io>`_. |br| If you like to contribute to the development of this open-source package, utilize the provided repo in below: |br| `<https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/>`_.

   .. grid-item-card:: STA2STAC

      .. image:: _static/resized_image_sta2stac.png
         :width: 100%
         :alt: TDS2STAC logo
         :align: center

      The STA2STAC package serves as a bridge between the SensorThings API(STA) and the SpatioTemporal Asset Catalog (STAC) standard, improving the process of making STA time-series data more Findable, Accessible, Interoperable, and Reusable (FAIR).

      .. tip:: To access information regarding the utilization of a STA2STAC, please refer to the STA2STAC documentation available at the following link:|br| `<https://sta2stac.readthedocs.io>`_. |br| If you like to contribute to the development of this open-source package, utilize the provided repo in below: |br| `<https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/>`_.


   .. grid-item-card:: INTAKE2STAC

      .. image:: _static/resized_image_intake2stac.png
         :width: 100%
         :alt: TDS2STAC logo
         :align: center


      `INTAKE2STAC <https://intake2stac.readthedocs.io/en/latest/>`_, a Python suite, streamlines the retrieval of dataset details from Amazon S3 via an INTAKE catalog and the creation of STAC-Catalog, -Collection, and -Items. This process is instrumental in elevating the FAIRness of environmental data on Amazon S3—making such data more discoverable (Findable), readily accessible (Accessible), seamlessly compatible (Interoperable) with other datasets, and easier to repurpose (Reusable).

      .. tip:: To access information regarding the utilization of a INTAKE2STAC, please refer to the INTAKE2STAC documentation available at the following link:|br| `<https://intake2stac.readthedocs.io>`_. |br| If you like to contribute to the development of this open-source package, utilize the provided repo in below: |br| `<https://codebase.helmholtz.cloud/cat4kit/ds2stac/intake2stac/>`_.

   .. grid-item-card:: INSUPDEL4STAC

      .. image:: _static/resized_image_insupdel4stac.png
         :width: 100%
         :alt: TDS2STAC logo
         :align: center


      STAC specification is a method of exposing spatial and temporal data collections in a standardized manner. Specifically, the STAC specification describes and catalogs spatiotemporal assets using a common structure. This package is desigend to manage ingestion, updating and and removing of STAC-Metadata toward either PostgreSQL schema and functions for STAC (pgSTAC) or STAC compliant FastAPI application (STAC-FASTAPI) services.

      .. tip:: To access information regarding the utilization of a INSUPDEL4STAC, please refer to the INSUPDEL4STAC documentation available at the following link:|br| `<https://insupdel4stac.readthedocs.io>`_. |br| If you like to contribute to the development of this open-source package, utilize the provided repo in below: |br| `<https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/>`_.


Data Servers / Services
=========================
A comprehensive manual designed to enhance your understanding of the employed data server/services within this project.

.. toctree::
   :maxdepth: 2

   data_server/index


About STAC
=========================
This document serves as a reference for learning about the STAC metadata standard.

.. toctree::
   :maxdepth: 2

   stac/index


Architechture
=========================



.. toctree::
   :maxdepth: 2

   algorithm/index

Contribution
=========================



.. toctree::
   :maxdepth: 2

   contribution/index
