..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0



=========
Algorithm
=========
.. image:: ../_static/ds2stac-algorithm.png
   :align: center
