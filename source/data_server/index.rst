..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _data_server:

======================
Data Servers / Services
======================

Three different data servers are available for the data of the project.

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Data Servers/Services

   thredds
   INTAKE
   SensorThingsapi
