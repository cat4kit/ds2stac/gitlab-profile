..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


=========
Intake (A general interface for loading data)
=========

Intake is a Python module that facilitates the loading of data from diverse formats and sources into commonly used data structures such as Pandas dataframes or Xarray DataSets, among others. Data is typically gathered and organized via catalogs, wherein each individual item within the catalog encompasses all the necessary information for effectively engaging with the data. Therefore, it eliminates the necessity for a prospective consumer to possess knowledge regarding the precise storage location or system, as well as the data format.
Additionally, the Intake software facilitates the creation of basic catalogs including data in various forms and sourced from other institutions. Therefore, it is of utmost importance in facilitating the establishment of replicable workflows involving data stored in decentralized storage systems. In the Cat4KIT framework, Intake catalogs are mostly utilized for high-volume models and remote sensing data, which are often stored on cloud-optimized storage systems like as S3-compliant object storage.
