..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


=========================
THREDDS Data Server (TDS)
=========================

THREDDS (The Thematic Real-time Environmental Distributed Data Services) is a data server for publishing Network Common Data Format (NetCDF) data via different interfaces, and it was developed by the `unidata community <https://www.unidata.ucar.edu>`_. Since NetCDF has become a de facto standard in many areas of environmental science, THREDDS-Server is frequently used to make scientific data available.

Catalogs are used internally in TDS to categorize information. Datasets can be linked statically or their dynamic creation using special tools called DatasetScans, which are all supported by these catalogues. Users can choose from a variety of interfaces in TDS to engage with the data in various ways. Some of the important interfaces inside the Cat4KIT-framework are presented here.


1. Simple remote access to the data in the NetCDF-files is made possible by `The Open-source Project for a Network Data Access Protocol (OPeNDAP) <https://www.opendap.org>`_. The Open Data Access Protocol (OpenDAP) is supported by a wide range of libraries for use in a variety of programming languages and environments. As a result, this interface is increasingly put to use to implement things like workflows that make use of distributed data stores.

2. To visualize information on a server, you can use OGC's `The Web Mapping Service <https://www.ogc.org/standard/wms/>`_ (WMS). It has become the de facto standard for displaying geospatial raster data thanks to its longevity and user-friendliness.


3. You can create `ISO 19115 <https://www.iso.org/standard/53798.html>`_  compliant metadata from NetCDF files using `ncISO <https://artifacts.unidata.ucar.edu/service/rest/repository/browse/unidata-all/EDS/nciso/>`_ . ISO 19115 is the metadata standard for many different communities, and as such, it includes a broad variety of qualities and information that (often) must be gathered manually. However, with ncISO, this data is automatically pulled from the NetCDFs, substantially streamlining the process of creating standardized metadata.

Despite these benefits, catalog services such as OAI-PMH (Open Archives Initiative Protocol for Metadata Harvesting) and OGC's Catalog Service for the Web (CSW) are not yet part of TDS. Therefore, we need to harvest the (meta)data from our THREDDS-servers into specialized catalog frameworks if we wish to provide search or filtering features.
