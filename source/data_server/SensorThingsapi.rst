..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0



=======================
SensorThings API (STA):
=======================


The Fraunhofer Open Source SensorThings API Server (FROST) serves as the authoritative implementation of the SensorThings API (STA) developed by the Open Geospatial Consortium (OGC). STA, which originates from the Internet of Things (IoT), is specifically designed to handle unidimensional data obtained from conventional sensor systems. The fundamental components of the STA comprise an interface, which encompasses a collection of commands for interacting with the (meta)data, and a specialized datamodel. The data model is designed based on the concept of "Things", as defined by STA, which refers to entities in both the physical and information realms that possess unique identification and can be seamlessly included into communication networks. Additionally, the framework offers provisions for the inclusion of entities such as \emph{Location}, which is directly associated with a Thing, as well as Sensor and ObservedProperty. The process of integrating a Thing, a Sensor, and a Observed Property results in the creation of a designated entity known as a DataStream. This entity serves as a receptacle for storing data.
